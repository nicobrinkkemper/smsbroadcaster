<?php

namespace Entities;

use MessageBird\Objects\Message;

class BinarySms extends Message
{

    /**
     * BinarySms constructor.
     * @param $originator
     * @param $recipients
     * @param $body
     * @param Udh $udh
     */
    public function __construct($originator, $recipients, $body, Udh $udh)
    {
        $this->originator = $originator;
        $this->recipients = $recipients;
        $this->typeDetails['udh'] = $udh->toString();
        $this->body = $body;
        $this->reference = null;
        $this->type = self::TYPE_BINARY;
        $this->datacoding = 'auto';
        // $this->scheduledDatetime = date(DATE_RFC3339);
    }

}