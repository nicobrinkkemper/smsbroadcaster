function Http () {
  function makeRequest(method,url,data) {
    data = data || '';
    return new Promise(function(resolve, reject) {
      var req = new XMLHttpRequest();
      req.open(method, url);
      // req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      req.onload = function() {
        if (req.status == 200) {
          resolve(req.response);
        }
        else {
          reject(Error(req.statusText));
        }
      };
      req.onerror = function() {
        reject(Error("Something went wrong ... "));
      };
      req.send(data);
    });
  }
  this.makeRequest = makeRequest;
}