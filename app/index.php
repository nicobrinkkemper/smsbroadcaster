<!DOCTYPE html>
<html>
<head>
  <title>Sms Broadcaster</title>
  <link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>
<div id="wrapper">
  <div id="form-container">
    <form id="sms-form"
          action="api/"
          method="post"
          onsubmit="return submitForm(this, function(){ document.getElementById('sms-status-submit').click() });">
      <p>
        <label id="sms-originator-label" for="sms-originator">Originator:</label>
        <input id="sms-originator"
               type="text"
               name="originator"
               title="Enter a number"
               value="MessageBird"/>
      </p>
      <p>
        <label id="sms-recipient-label" for="sms-recipent">Recipient:</label>
        <input id="sms-recipient"
               type="text"
               name="recipients[]"
               title="Enter a number"
               value=""/>
      </p>
      <p id="sms-body-container">
        <label id="sms-body-label" for="sms-body">Body:</label>
        <textarea id="sms-body" name="body"></textarea>
      </p>
      <p id="sms-submit-container">
        <input id="sms-submit" type="submit" value="Queue and send"/>
      </p>
    </form>
    <form id="sms-status-form"
          style="visibility: hidden"
          action="queue/"
          method="get"
          onsubmit="return submitForm(this, null);">
      <p id="sms-submit-container">
        <input id="sms-status-submit" type="submit" value="Send"/>
      </p>
    </form>
  </div>
</div>
<script src="assets/http.js"></script>
<script src="assets/app.js"></script>
</body>
</html>
