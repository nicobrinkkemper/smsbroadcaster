<?php
require_once '../bootstrap.php';

if(empty($_SESSION['queue']))
    die('nothing to do');

if($_SESSION['busy'])
    die('busy');

if ($_SERVER['REQUEST_METHOD'] !== 'GET')
    die('Request method must be get');

if(!getenv('MB_TEST_KEY') || !getenv('MB_KEY'))
    die('no messagebird key set in .env');

$messageBird = new \MessageBird\Client(getenv('MB_KEY')); // See .env file
$data = [
    'results' => [],
    'messages' => []
];

foreach($_SESSION['queue'] as $key => $jsonSms){
    $_SESSION['busy'] = true;
    $sms = json_decode($jsonSms, true);
    try {
        $MessageResult = $messageBird->messages->create($sms);
        $data['results'][$key] = json_encode($MessageResult);
        $data['messages'][] = "message send";
    } catch (\MessageBird\Exceptions\AuthenticateException $e){
        $data['messages'][] = "wrong login";
    } catch (\MessageBird\Exceptions\BalanceException $e) {
        $data['messages'][] = "no balance";
    } catch (\Exception $e) {
        $data['messages'][] = $e->getMessage();
    }
    usleep(1000001);
    unset($_SESSION['queue'][$key]);
}
$_SESSION['busy'] = false;

echo json_encode($data);
