<?php

namespace Entities;

use MessageBird\Objects\Message;

class Sms extends Message
{
    public $originator;
    public $recipients;
    public $body;
    public $datacoding;

    /**
     * Sms constructor.
     * @param $originator
     * @param $recipients
     * @param $body
     * @param string $datacoding
     */
    public function __construct($originator, $recipients, $body, $datacoding = 'auto')
    {
        $this->originator = $originator;
        $this->recipients = $recipients;
        $this->body = $body;
        $this->datacoding = $datacoding;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        if (!is_string($this->originator) || $this->originator == "")
            return false;
        if (!is_array($this->recipients) || !is_numeric($this->recipients[0]))
            return false;
        if (!is_string($this->body) || $this->body == "")
            return false;
        return true;
    }

    /**
     * @return bool
     */
    public function shouldSendBinary()
    {
        return strlen($this->body) > 160;
    }

    /**
     * @return array
     */
    public function toBinarySms()
    {
        $bodyChunks = str_split($this->body, 153);
        $ref = mt_rand(1, 255);
        $count = sizeof($bodyChunks);
        $array = [];
        $i = 1;
        foreach ($bodyChunks as $bodyChunk) {
            $array[$i] = new BinarySms(
                $this->originator,
                $this->recipients,
                $bodyChunk,
                new Udh($i, $ref, $count)
            );
            $i++;
        }
        return $array;
    }
}