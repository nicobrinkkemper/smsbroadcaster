<?php

namespace Entities;


class Udh
{
    private $index, $count;

    /**
     * Udh constructor.
     * @param $index
     * @param $reference
     * @param $count
     */
    public function __construct($index, $reference, $count)
    {
        $this->index = $index;
        $this->reference = $reference;
        $this->count = $count;
    }

    /**
     * See: https://en.wikipedia.org/wiki/Concatenated_SMS
     *
     * @return string
     */
    public function toString()
    {
        $binary = array(
            'udh_length' => '05',
            'identifier' => '00',
            'header_length' => '03',
            'reference' => static::dechexStr($this->reference),
            'count' => static::dechexStr($this->count),
            'index' => static::dechexStr($this->index)
        );
        return implode('', $binary);
    }

    /**
     * @usage convert decimal to zerofilled hexadecimal
     * @param integer $ref
     * @return 2 digit hexa-decimal in string format
     */
    private static function dechexStr($ref)
    {
        return ($ref <= 15) ? '0' . dechex($ref) : dechex($ref);
    }
}