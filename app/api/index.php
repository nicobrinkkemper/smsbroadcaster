<?php
require_once '../bootstrap.php';

if ($_SERVER['REQUEST_METHOD'] !== 'POST')
    die('Request method must be post');


$sms = new \Entities\Sms(
    $_POST['originator'],
    $_POST['recipients'],
    $_POST['body']
);

if (!$sms->isValid())
    die('Sms is invalid');

if ($sms->shouldSendBinary())
    foreach ($sms->toBinarySms() as $binarySms)
        $_SESSION['queue'][] = json_encode($binarySms);
else
    $_SESSION['queue'][] = json_encode($sms);

## return
echo json_encode($_SESSION['queue']);