<?php
require 'vendor/autoload.php';
session_start();
if(!is_array($_SESSION['queue'])){
    $_SESSION['queue'] = array();
}
if(is_null($_SESSION['busy'])){
    $_SESSION['busy'] = false;
}
$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();