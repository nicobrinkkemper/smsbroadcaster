Example of MessageBird API implementation. Sends a concatenated SMS with messages over 160 characters.

## Instructions
* Run `docker-compose up`
* Copy or rename .env.example to .env and fill in your API key
* Navigate to localhost:8080