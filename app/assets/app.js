function submitForm(context, callback){
  var http = new Http();
  var data = new FormData(context);
  http.makeRequest(context.method, context.action, data).then(
    function () {
      if(callback!=null) {
        callback();
      }
    }, function (error) {
      console.error("Failed!", error);
    }
  );
  return false;
}